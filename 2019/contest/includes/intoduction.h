#ifndef INTODUCTION_H
#define INTODUCTION_H

#include <fstream>
#include <vector>
#include <string>


class Introduction {
    public:
        static int sum(int left, int right) {
            return left + right;
        }

        static int ab_runner(int argc, char* argv[]) {
            if (argc != 3) {
                return EXIT_FAILURE;
            }

            std::ifstream infile(argv[1]);

            if (! infile.is_open()) {
                return EXIT_FAILURE;
            }

            int count;
            int left, right;

            std::vector<int> ans;

            infile >> count;

            for (int i = 0; i < count; ++i) {
               infile >> left >> right;

               ans.push_back(sum(left, right));
            }

            std::ofstream outfile(argv[2]);

            for (int i = 0; i < count; ++i) {
                outfile << ans[i] << std::endl;
            }

            return EXIT_SUCCESS;
        }



        static std::vector<int> motif(const std::string& s, const std::string& t) {
            std::vector<int> ans;

            std::string::size_type pos = 0;

            while ((pos = s.find(t, pos)) != std::string::npos) {
                ans.push_back(++pos);
            }

            return std::move(ans);
        }

        static int motif_runner(int argc, char* argv[]) {
            if (argc != 3) {
                return EXIT_FAILURE;
            }

            std::ifstream infile(argv[1]);

            if (! infile.is_open()) {
                return EXIT_FAILURE;
            }

            int count;
            std::string s, t;

            std::vector<std::vector<int>> ans;

            infile >> count;

            for (int i = 0; i < count; ++i) {
               infile >> s >> t;

               ans.push_back(motif(s, t));
            }

            std::ofstream outfile(argv[2]);

            for (int i = 0; i < count; ++i) {
                for (int ansi: ans[i]) {
                    outfile << ansi << ' ';
                }
                outfile << std::endl;
            }

            return EXIT_SUCCESS;
        }
};

#endif // INTODUCTION_H
