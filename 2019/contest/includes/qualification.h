#ifndef QUALIFICATION_H
#define QUALIFICATION_H


#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <limits>
#include <cmath>

class Qualification {
    public:
        static double bee_population_stupid(double n1, double a, double b) {
            double limit = n1;

            for (int i = 0; i < 1e5; ++i) {
                limit = a * limit - b * limit * limit;

                if (limit < 0.) {
                    return 0.;
                } else if (limit > 1e5) {
                    return -1.;
                }
            }

            return limit;
        }

        static double bee_population(double n1, double a, double b) {
            if (b == 0) {
                if (a > 1.) {
                    return -1.;
                } else if ( a < 1.) {
                    return 0.;
                } else {
                    return n1;
                }
            }

            double limit = n1;

            for (int i = 0; i < 1e1; ++i) {
                limit = a * limit - b * limit * limit;

                if (limit < 0.) {
                    return 0.;
                } else if (limit > 1e5) {
                    return -1.;
                }
            }

            double qlimit = (a - 1.) / b;

            if ((qlimit == limit) && (qlimit > 0)) {
                return qlimit;
            }
            else if ((qlimit < limit) && (qlimit > 0)) {
                if (1 < a  - b * limit) {
                    return -1;
                } else {
                    return qlimit;
                }
            } else if ((qlimit > limit) && (qlimit > 0)) {
                if (1 < a - b * limit) {
                    return qlimit;
                } else {
                    return 0.;
                }
            } else {
                if (1 < a - b * limit) {
                    return -1.;
                } else {
                    return 0.;
                }
            }
        }

        static int bee_population_runner(int argc, char* argv[]) {
            if (argc != 3) {
                return EXIT_FAILURE;
            }

            std::ifstream infile(argv[1]);

            if (! infile.is_open()) {
                return EXIT_FAILURE;
            }

            int count;
            double n1, a, b;

            std::vector<double> ans;

            infile >> count;

            for (int i = 0; i < count; ++i) {
               infile >> n1 >> a >> b;

               ans.push_back(bee_population(n1, a, b));
            }

            std::ofstream outfile(argv[2]);

            int precision = std::numeric_limits<double>::max_digits10;

            for (int i = 0; i < count; ++i) {
                outfile << std::setprecision(precision) << ans[i] << std::endl;
            }

            return EXIT_SUCCESS;
        }

        static double update_binominal(unsigned n, unsigned k, double value) {
            unsigned numer = n == k - 1 ? 1 : n - k + 1;
            unsigned denom = k;

            return value * numer / denom;
        }

        static double get_simple_mode_term(int valid_read_count, int error_read_count) {
            if (valid_read_count > error_read_count) {
                return 0.;
            } else if (valid_read_count == error_read_count) {
                return 0.5;
            } else {
                return 1.;
            }
        }

        static double get_complex_mode_term(int valid_read_count, int error_read_count) {
            if (valid_read_count > error_read_count) {
                return 0.;
            } else {
                double error = 0;
                double prob;

                double bprob = std::pow(1. / 3, error_read_count);

                for (int type_one = 0; type_one <= error_read_count; ++type_one) {
                    prob = bprob;

                    for (int type_two = 0; type_two <= error_read_count - type_one; ++type_two) {
                        int type_three = error_read_count - type_one - type_two;

                        double term = prob;

                        int max_error_read = std::max(type_one, std::max(type_two, type_three));
                        int count = (type_one==max_error_read) + (type_two==max_error_read) + (type_three==max_error_read);

                        if (valid_read_count > max_error_read) {
                            term *= 0.;
                        } else if (valid_read_count == max_error_read) {
                            term *= (double)count / (count + 1);
                        } else {
                            term *= 1.;
                        }

                        if (type_two < error_read_count - type_one) {
                            prob = update_binominal(type_two+type_three, type_two+1, prob);
                        }

                        error += term;
                    }

                    if (type_one < error_read_count) {
                        bprob = update_binominal(error_read_count, type_one+1, bprob);
                    }
                }

                return error;
            }
        }

        static double get_error_ratio(int read_count, double p, bool is_simple) {
            if (read_count == 0)
              return 0.75;

            double error_ratio = 0;

            double prob = std::pow(1 - p, read_count);
            double step_prob = p / (1 - p);

            for (int error_read_count = 0; error_read_count <= read_count; ++error_read_count) {
                int valid_read_count = read_count - error_read_count;

                double term = prob;

                if (error_read_count < read_count) {
                    prob = update_binominal(read_count, error_read_count+1, prob);
                    prob *= step_prob;
                }

                if (is_simple) {
                  term *= get_simple_mode_term(valid_read_count, error_read_count);
                } else {
                  term *= get_complex_mode_term(valid_read_count, error_read_count);
                }

                error_ratio += term;
            }

            return error_ratio;
        }

        static double sequencing(unsigned L, unsigned n, double p, unsigned k, bool is_simple=true) {
            double read_prob = (double)n / L;
            double skip_prob = (double)(L - n) / L;
            double step_prob = (double)n / (L - n);

            double prob = L * std::pow(skip_prob, k);
            double expected_error = 0;

            for (int i = 0; i <= k; ++i) {
                double term = prob;
                term *= get_error_ratio(i, p, is_simple);

                if (i < k) {
                    prob = update_binominal(k, i+1, prob);
                    prob *= step_prob;
                }

                expected_error += term;
            }

            return expected_error;
        }

        static int sequencing_runner(int argc, char* argv[]) {
            if (argc != 3) {
                return EXIT_FAILURE;
            }

            std::ifstream infile(argv[1]);

            if (! infile.is_open()) {
                return EXIT_FAILURE;
            }

            int count;
            unsigned L;
            unsigned n;
            double p;
            unsigned k;

            std::vector<double> ans;

            infile >> count;

            for (int i = 0; i < count; ++i) {
               infile >> L >> n >> p >> k;

               ans.push_back(sequencing(L, n, p, k, false));
            }

            std::ofstream outfile(argv[2]);

            for (int i = 0; i < count; ++i) {
                outfile << std::fixed << std::setprecision(15) << ans[i] << std::endl;
            }

            return EXIT_SUCCESS;
        }

};

#endif // QUALIFICATION_H
