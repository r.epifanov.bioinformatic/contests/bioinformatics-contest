#include <iostream>

#include "intoduction.h"
#include "qualification.h"


using namespace std;

int main(int argc, char* argv[]) {
    return Qualification::sequencing_runner(argc, argv);
}
