TEMPLATE = app
CONFIG += console c++11

QMAKE_CC = gcc-5
QMAKE_CXX = g++-5

INCLUDEPATH += includes
DEPENDPATH += includes

SOURCES += \
        main.cpp

HEADERS += \
    includes/intoduction.h \
    includes/qualification.h
