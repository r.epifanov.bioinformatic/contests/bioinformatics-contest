TEMPLATE = app
CONFIG += console c++11

QMAKE_CC = gcc-5
QMAKE_CXX = g++-5

INCLUDEPATH += googletest/include googletest ../contest/includes
DEPENDPATH += googletest/include googletest ../contest/includes

SOURCES += \
    main.cpp \
    googletest/src/gtest.cc \
    googletest/src/gtest-all.cc \
    googletest/src/gtest-death-test.cc \
    googletest/src/gtest-filepath.cc \
    googletest/src/gtest-matchers.cc \
    googletest/src/gtest-port.cc \
    googletest/src/gtest-printers.cc \
    googletest/src/gtest-test-part.cc \
    googletest/src/gtest-typed-test.cc

LIBS += -L/usr/local/lib
