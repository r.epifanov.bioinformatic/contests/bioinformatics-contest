#include <gtest/gtest.h>

#include "intoduction.h"
#include "qualification.h"


#include <string>
#include <vector>
#include <fstream>

TEST (test_motif, simple) {
    std::string s = "GATATATGCATATACTTTAT";
    std::string t = "TAT";


    std::vector<int> output = Introduction::motif(s, t);

    std::vector<int> expected = {3, 5, 11, 18};

    ASSERT_EQ(output, expected);
}

TEST (test_bee_population, one) {
    double n1 = 0.5, a = 1, b = 1;

    double output = Qualification::bee_population(n1, a, b);
    double expected = 0;

    ASSERT_NEAR(output, expected, 1e-4);
}

TEST (test_bee_population, two) {
    double n1 = 10, a = 1, b = 2;

    double output = Qualification::bee_population(n1, a, b);
    double expected = 0;

    ASSERT_NEAR(output, expected, 1e-4);
}

TEST (test_bee_population, three) {
    double n1 = 0.5, a = 1.5, b = 1;

    double output = Qualification::bee_population(n1, a, b);
    double expected = 0.5;

    ASSERT_NEAR(output, expected, 1e-4);
}

TEST (test_bee_population, four) {
    double n1 = 0.076, a = 0.364, b = 0.308;

    double output = Qualification::bee_population(n1, a, b);
    double expected = 0.;

    ASSERT_NEAR(output, expected, 1e-4);
}

TEST (test_bee_population, five) {
    double n1 = 0.5, a = 3., b = 1.;

    double output = Qualification::bee_population(n1, a, b);
    double expected = 2.;

    ASSERT_NEAR(output, expected, 1e-4);
}

TEST (test_bee_population, file) {
    std::ifstream infile("bee_tests.txt");

    int count;
    double n1, a, b;

    infile >> count;

    for (int i = 0; i < count; ++i) {
        infile >> n1 >> a >> b;

        auto output = Qualification::bee_population(n1, a, b);
        auto expected = Qualification::bee_population_stupid(n1, a, b);

        ASSERT_NEAR(output, expected, 1e-2) << i << ' ' << n1 << ' ' << a << ' ' << b;
    }
}

TEST (test_update_binominal, three) {
    unsigned base = 3;
    double binominal = 1;

    std::vector<int> expecteds = {1, 3, 3, 1};

    for (int i = 1; i < base + 1; ++i) {
        binominal = Qualification::update_binominal(base, i, binominal);
        ASSERT_EQ(binominal, expecteds[i]) << i;
    }
}

TEST (test_update_binominal, six) {
    unsigned base = 6;
    double binominal = 1;

    std::vector<int> expecteds = {1, 6, 15, 20, 15, 6, 1};

    for (int i = 1; i < base + 1; ++i) {
        binominal = Qualification::update_binominal(base, i, binominal);
        ASSERT_EQ(binominal, expecteds[i]) << i;
    }
}

TEST (test_sequencing_simple, one) {
    unsigned L = 4;
    unsigned n = 2;
    unsigned k = 1;
    double p = 0.0;

    auto output = Qualification::sequencing(L, n, p, k);
    auto expected = 1.5;

    ASSERT_NEAR(output, expected, 1e-6);
}

TEST (test_sequencing_simple, two) {
    unsigned L = 4;
    unsigned n = 2;
    unsigned k = 2;
    double p = 0.0;

    auto output = Qualification::sequencing(L, n, p, k);
    auto expected = 0.75;

    ASSERT_NEAR(output, expected, 1e-6);
}

TEST (test_sequencing_simple, three) {
    unsigned L = 10;
    unsigned n = 3;
    unsigned k = 4;
    double p = 0.05;

    auto output = Qualification::sequencing(L, n, p, k);
    auto expected = 2.14491825;

    ASSERT_NEAR(output, expected, 1e-6);
}

TEST (test_get_error_ratio, zero) {
    int read_count = 0;
    double p = 0.05;

    auto output = Qualification::get_error_ratio(read_count, p, true);
    auto expected = 0.75;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_error_ratio, one) {
    int read_count = 1;
    double p = 0.05;

    auto output = Qualification::get_error_ratio(read_count, p, true);
    auto expected = p;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_error_ratio, two) {
    int read_count = 2;
    double p = 0.05;

    auto output = Qualification::get_error_ratio(read_count, p, true);
    auto expected = (1-p)*(1-p)*0 + 2*(1-p)*p*0.5 + p*p;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_sequencing_complex, one) {
    unsigned L = 10;
    unsigned n = 3;
    unsigned k = 4;
    double p = 0.05;

    auto output = Qualification::sequencing(L, n, p, k, false);
    auto expected = 2.1433536;

    ASSERT_NEAR(output, expected, 1e-6);
}

TEST (test_sequencing_complex, two) {
    unsigned L = 4;
    unsigned n = 2;
    unsigned k = 3;
    double p = 0.05;

    auto output = Qualification::sequencing(L, n, p, k, false);

    double read_prob = (double)n / L;
    double skip_prob = (double)(L - n) / L;

    auto expected = L * (1*std::pow(skip_prob, 3)*std::pow(read_prob, 0)*Qualification::get_error_ratio(0, p, false)+
                         3*std::pow(skip_prob, 2)*std::pow(read_prob, 1)*Qualification::get_error_ratio(1, p, false)+
                         3*std::pow(skip_prob, 1)*std::pow(read_prob, 2)*Qualification::get_error_ratio(2, p, false)+
                         1*std::pow(skip_prob, 0)*std::pow(read_prob, 3)*Qualification::get_error_ratio(3, p, false));

    ASSERT_NEAR(output, expected, 1e-6);
}

TEST (test_get_error_ratio_complex, one) {
    int read_count = 1;
    double p = 0.05;
    double ip = 1 - p;

    auto output = Qualification::get_error_ratio(read_count, p, false);

    auto expected = ip * 0 + p * 1;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_error_ratio_complex, two) {
    int read_count = 2;
    double p = 0.05;
    double ip = 1 - p;

    auto output = Qualification::get_error_ratio(read_count, p, false);

    auto expected = ip*ip * 0 + 2*ip*p *0.5 + p*p*1;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_complex_mode_term, one) {
    int valid_read_count = 2;
    int error_read_count = 1;

    auto output = Qualification::get_complex_mode_term(valid_read_count, error_read_count);
    auto expected = 0;

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_complex_mode_term, two) {
    int valid_read_count = 1;
    int error_read_count = 2;

    auto output = Qualification::get_complex_mode_term(valid_read_count, error_read_count);
    auto expected = std::pow(1./3, 2) * (1 * 1 + 2 * (2. / 3) + 1 * 1 + 2 * (2. / 3) + 2 * (2. / 3) + 1 * 1);

    ASSERT_DOUBLE_EQ(output, expected);
}

TEST (test_get_complex_mode_term, three) {
    int valid_read_count = 1;
    int error_read_count = 3;

    auto output = Qualification::get_complex_mode_term(valid_read_count, error_read_count);
    auto expected = std::pow(1./3, 3) * (1 * 1 + 3 * 1 + 3 * 1 + 1 * 1 + 3 * 1 + 6 * 0.75 + 3 * 1 + 3 * 1 + 3 * 1 +  1 * 1);

    ASSERT_DOUBLE_EQ(output, expected);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
